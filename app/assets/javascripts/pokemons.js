// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).on('ready page:load', function(){

  $.ajax({
    dataType: 'json',
    url: '/pokemons',
    type: 'GET',
    success: function(data){
      if (data){
        var source = $('#pokemon-list-item').html();
        var template = Handlebars.compile(source);

        for (var i = 0; i < data.length; i++) {
          var html = template(data[i]);
          $('.pokemon-list').append(html);
        }
      }
    }
  });

  $('.pokemon-link').click(function(){
    $.ajax({
      dataType: 'html',
      url: '/pokemons/:id',
      type: 'GET'
    })
  });

});
