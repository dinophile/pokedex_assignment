class PokemonsController < ApplicationController
  def index
    @pokemons = Pokemon.all

    respond_to do |format|
      format.html
      format.json {render json: @pokemons}
    end

  end

  def show
    @pokemon = Pokemon.find(params[:id])

    respond_to do |format|
      format.html {render :show}
      format.json {render json: {name: @pokemon.name, image_url: @pokemon.image_url}}
    end

  end

end
