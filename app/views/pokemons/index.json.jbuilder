json.pokemons @pokemons do |pokemon|
  json.name pokemon.name
  json.image_url pokemon.image_url
end
